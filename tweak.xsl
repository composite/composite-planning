<?xml version='1.0'?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:param name='section.autolabel' select='1'/>
  <xsl:param name='toc.max.depth' select='2'/>
  <xsl:param name='xref.with.number.and.title' select='0'/>
  <xsl:param name='html.stylesheet'>specs_style.css</xsl:param>
</xsl:stylesheet>
