
XMLTO_OPTS = -m tweak.xsl

TARGETS = specification.html \
	faq.html

all: $(TARGETS)
	cd plugins/sampler/1 && make

# Warning: targets are hard-coded here.
valid:
	xmllint --valid specification.html > /dev/null
	xmllint --valid faq.html > /dev/null

clean:
	-rm -f $(TARGETS) *~ *.docbook_validated

%.html: %.docbook %.docbook_validated tweak.xsl
	xmlto xhtml-nochunks $(XMLTO_OPTS) $<

%.docbook_validated: %.docbook
	xmllint --noout --valid $^
	touch $@

